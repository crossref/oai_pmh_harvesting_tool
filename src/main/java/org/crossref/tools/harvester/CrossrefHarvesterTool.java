//
// Decompiled by Procyon v0.5.36
//

package org.crossref.tools.harvester;

import java.net.URLEncoder;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.StringTokenizer;
import java.util.Collections;
import java.util.ArrayList;
import java.net.MalformedURLException;
import java.net.URL;
import java.io.File;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;
import java.text.MessageFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Iterator;
import org.jdom.filter.Filter;
import org.jdom.filter.ElementFilter;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.Element;
import org.jdom.Document;
import java.io.InputStream;
import org.apache.commons.httpclient.HttpMethod;
import java.io.IOException;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.HttpClient;
import java.io.OutputStream;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.ArrayDeque;
import java.io.PrintStream;
import java.util.Map;
import java.util.Deque;
import java.text.SimpleDateFormat;

public class CrossrefHarvesterTool
{
    private static final SimpleDateFormat LOG_DATE_FORMAT;
    private final Deque<HarvestRequest> queue;
    private String outFile;
    private long attemptWaitDuration;
    private int maximumAttempts;
    private String accessToken;
    private final Map<String, Handler> verbToHandler;
    private PrintStream out;

    public CrossrefHarvesterTool() {
        this.queue = new ArrayDeque<HarvestRequest>();
        this.attemptWaitDuration = 5000L;
        this.maximumAttempts = 7;
        this.accessToken = null;
        this.verbToHandler = new HashMap<String, Handler>();
    }

    public static void main(final String... args) throws Exception {
        final CrossrefHarvesterTool tool = new CrossrefHarvesterTool();
        for (int i = 0; i < args.length; ++i) {
            if ("--output".equals(args[i])) {
                tool.setOutput(args[i + 1]);
                ++i;
            }
            else if ("--attempt-wait".equals(args[i])) {
                tool.setAttemptWait(Long.parseLong(args[i + 1]));
                ++i;
            }
            else if ("--maximum-attempts".equals(args[i])) {
                tool.setMaximumAttempts(Integer.parseInt(args[i + 1]));
                ++i;
            }
            else if ("--access-token".equals(args[i])) {
                tool.setAccessToken(args[i + 1]);
                ++i;
            }
            else if ("--version".equals(args[i])) {
                System.out.printf("%s 1.1.0\n", CrossrefHarvesterTool.class.getCanonicalName());
                System.exit(0);
            }
            else if (args[i].startsWith("-")) {
                if (!"--help".equals(args[i])) {
                    System.out.printf("error: unknown command line argument \"%s\"\n", args[i]);
                }
                System.out.printf("usage: %s --output file --access-token token --attempt-wait milliseconds --maximum-attempts count url...\n", CrossrefHarvesterTool.class.getCanonicalName());
                System.exit(1);
            }
            else {
                tool.addUrl(args[i]);
            }
        }
        tool.initialize();
        tool.execute();
    }

    public void setAttemptWait(final long duration) {
        this.attemptWaitDuration = duration;
    }

    public void setMaximumAttempts(final int maximumFailures) {
        this.maximumAttempts = maximumFailures;
    }

    public void setOutput(final String outFile) {
        this.outFile = outFile;
    }

    public void setAccessToken(final String accessToken) {
        this.accessToken = accessToken;
    }

    public void addUrl(final String url) {
        this.queue.addLast(new HarvestRequest(url));
    }

    public void initialize() throws Exception {
        this.verbToHandler.put("ListSets", new ListSetsBodyHandler());
        this.verbToHandler.put("ListIdentifiers", new ListIdentifiersBodyHandler());
        this.verbToHandler.put("ListRecords", new ListRecordsBodyHandler());
    }

    public void execute() throws Exception {
        this.out = ((this.outFile == null || "-".equals(this.outFile)) ? System.out : new PrintStream(new FileOutputStream(this.outFile), false, "UTF-8"));
        try {
            this.harvest(this.queue);
        }
        catch (Exception e) {
            this.log(Level.ERROR, e, "unable to complete harvest", new Object[0]);
        }
        finally {
            this.out.close();
        }
    }

    private void harvest(final Deque<HarvestRequest> queue) {
        final HttpClient client = new HttpClient();
        while (!queue.isEmpty()) {
            final HarvestRequest harvest = queue.removeFirst();
            this.log(Level.INFO, "harvesting {0}", harvest);
            HttpMethod method = null;
            try {
                if (harvest.getAttemptsCount() > 1) {
                    if (harvest.getAttemptsCount() >= this.maximumAttempts) {
                        this.log(Level.ERROR, "maximum attempts exceeded {0}", harvest);
                        break;
                    }
                    this.log(Level.WARNING, "waiting before {0}", harvest);
                    Thread.sleep(this.attemptWaitDuration);
                }
                method = (HttpMethod)new GetMethod(harvest.getUrl());
                method.setFollowRedirects(true);
                method.addRequestHeader("Accept-Language", "en-us,en");
                method.addRequestHeader("Accept-Charset", "UTF-8");
                if (this.accessToken != null) {
                    method.addRequestHeader("Authorization", "Bearer " + this.accessToken);
                }
                this.out.printf("request.url: %s\n", harvest.getUrl());
                this.out.printf("request.begin: %1$tF %1$tT,%1$tL%1$tz\n", System.currentTimeMillis());
                int status;
                try {
                    status = client.executeMethod(method);
                }
                finally {
                    this.out.printf("request.end: %1$tF %1$tT,%1$tL%1$tz\n", System.currentTimeMillis());
                }
                switch (status) {
                    case 200: {
                        try (final InputStream in = method.getResponseBodyAsStream()) {
                            final Document document = this.buildDocument(in);
                            if (document != null) {
                                final Element error = this.findDescendant(document.getRootElement(), "error");
                                if (error != null) {
                                    final String code = error.getAttributeValue("code");
                                    final String message = error.getTextNormalize();
                                    this.out.printf("request.error.code: %s\n", (code != null) ? code : "");
                                    this.out.printf("request.error.message: %s\n", (message != null) ? message : "");
                                }
                                else {
                                    final Element request = this.findDescendant(document.getRootElement(), "request");
                                    if (request != null) {
                                        final String verb = request.getAttributeValue("verb");
                                        final Handler handler = this.verbToHandler.get(verb);
                                        if (handler != null) {
                                            this.out.printf("handling.begin: %1$tF %1$tT,%1$tL%1$tz\n", System.currentTimeMillis());
                                            try {
                                                handler.handle(document);
                                            }
                                            finally {
                                                this.out.printf("handling.end: %1$tF %1$tT,%1$tL%1$tz\n", System.currentTimeMillis());
                                            }
                                            String resumptionToken = request.getAttributeValue("resumptionToken");
                                            if (resumptionToken == null) {
                                                final Element e = this.findDescendant(document.getRootElement(), "resumptionToken");
                                                if (e != null) {
                                                    resumptionToken = e.getTextTrim();
                                                    if (resumptionToken.length() == 0) {
                                                        resumptionToken = null;
                                                    }
                                                }
                                            }
                                            if (resumptionToken != null) {
                                                this.log(Level.INFO, "resuming using {0}", resumptionToken);
                                                final SimpleUrlBuilder b = new SimpleUrlBuilder(harvest.getUrl());
                                                b.setParameter("resumptionToken", resumptionToken);
                                                queue.addLast(new HarvestRequest(b.build()));
                                            }
                                        }
                                        else {
                                            this.log(Level.WARNING, "no handler for verb {0}", verb);
                                        }
                                    }
                                }
                            }
                        }
                        break;
                    }
                    default: {
                        this.log(Level.WARNING, "retrying {0}", harvest);
                        harvest.incAttemptsCount();
                        queue.addFirst(harvest);
                        break;
                    }
                }
            }
            catch (IOException e2) {
                this.log(Level.WARNING, e2, "retrying {0}", harvest);
                harvest.incAttemptsCount();
                queue.addFirst(harvest);
            }
            catch (InterruptedException e3) {
                this.log(Level.INFO, e3, "interrupted harvested {0}", harvest);
                Thread.currentThread().interrupt();
            }
            finally {
                if (method != null) {
                    method.releaseConnection();
                }
            }
            this.log(Level.INFO, "harvested {0}", harvest);
        }
    }

    private Document buildDocument(final InputStream in) {
        try {
            final Document document = new SAXBuilder().build(in);
            return document;
        }
        catch (JDOMException | IOException ex) {
            this.log(Level.ERROR, ex, "unable to parse XML", new Object[0]);
            return null;
        }
    }

    private String findDescendantValue(final Element e, final String name, final String defaultValue) {
        final Element f = this.findDescendant(e, name);
        if (f != null) {
            final String value = f.getTextTrim();
            if (value != null) {
                return value;
            }
        }
        return defaultValue;
    }

    private Element findDescendant(final Element e) {
        if (e != null) {
            final ElementFilter filter = new ElementFilter();
            final Iterator i = e.getDescendants((Filter)filter);
            if (i.hasNext()) {
                return (org.jdom.Element) i.next();
            }
        }
        return null;
    }

    private Element findDescendant(final Element e, final String name) {
        if (e != null) {
            final ElementFilter filter = new ElementFilter(name);
            final Iterator i = e.getDescendants((Filter)filter);
            if (i.hasNext()) {
                return (org.jdom.Element) i.next();
            }
        }
        return null;
    }

    private List<Element> findDescendants(final Element e, final String name) {
        final List<Element> found = new LinkedList<Element>();
        if (e != null) {
            final ElementFilter filter = new ElementFilter(name);
            final Iterator i = e.getDescendants((Filter)filter);
            while (i.hasNext()) {
                found.add((org.jdom.Element) i.next());
            }
        }
        return found;
    }

    private void log(final Level level, final Throwable throwable, final String message, final Object... parameters) {
        this.log(level, message, parameters);
        throwable.printStackTrace(System.out);
    }

    private void log(final Level level, final String message, final Object... parameters) {
        System.out.print(CrossrefHarvesterTool.LOG_DATE_FORMAT.format(new Date()));
        System.out.print(" ");
        System.out.print(level.name());
        System.out.print(" ");
        System.out.println(MessageFormat.format(message, parameters));
    }

    static {
        LOG_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSZ");
    }

    private enum Level
    {
        INFO,
        WARNING,
        ERROR;
    }

    static class HarvestRequest
    {
        private final String url;
        private final long created;
        private long updated;
        private int attemptsCount;

        public HarvestRequest(final String url) {
            this.url = url;
            this.created = System.currentTimeMillis();
        }

        public String getUrl() {
            return this.url;
        }

        public long getCreated() {
            return this.created;
        }

        public long getUpdated() {
            return this.updated;
        }

        public void incAttemptsCount() {
            ++this.attemptsCount;
            this.updated = System.currentTimeMillis();
        }

        public int getAttemptsCount() {
            return this.attemptsCount;
        }

        @Override
        public String toString() {
            return (this.updated > 0L) ? String.format("url=%1$s; created=%2$tF %2$tT,%2$tL%2$tz; updated=%3$tF %3$tT,%3$tL%3$tz; count=%4$d;", this.url, this.created, this.updated, this.attemptsCount) : String.format("url=%1$s; created=%2$tF %2$tT,%2$tL%2$tz", this.url, this.created);
        }
    }

    class ListSetsBodyHandler implements Handler<Document>
    {
        int count;

        ListSetsBodyHandler() {
            this.count = 0;
        }

        @Override
        public void handle(final Document document) {
            final Element root = document.getRootElement();
            for (final Element set : CrossrefHarvesterTool.this.findDescendants(root, "set")) {
                final String setSpec = set.getChildText("setSpec", set.getNamespace());
                final String setName = set.getChildText("setName", set.getNamespace());
                CrossrefHarvesterTool.this.out.printf("set.count: %d\n", ++this.count);
                CrossrefHarvesterTool.this.out.printf("set.setspec: %s\n", setSpec);
                CrossrefHarvesterTool.this.out.printf("set.setName: %s\n", setName);
                CrossrefHarvesterTool.this.out.flush();
            }
        }
    }

    class ListIdentifiersBodyHandler implements Handler<Document>
    {
        int count;

        ListIdentifiersBodyHandler() {
            this.count = 0;
        }

        @Override
        public void handle(final Document document) {
            final Element root = document.getRootElement();
            for (final Element header : CrossrefHarvesterTool.this.findDescendants(root, "header")) {
                final String identifier = CrossrefHarvesterTool.this.findDescendantValue(header, "identifier", "info:doi/");
                final String datestamp = CrossrefHarvesterTool.this.findDescendantValue(header, "datestamp", "");
                final String doi = SimpleUrlBuilder.decode(identifier).substring(9);
                CrossrefHarvesterTool.this.out.printf("identifier.count: %d\n", ++this.count);
                CrossrefHarvesterTool.this.out.printf("identifier.datestamp: %s\n", datestamp);
                CrossrefHarvesterTool.this.out.printf("identifier.identifier: %s\n", identifier);
                CrossrefHarvesterTool.this.out.printf("identifier.doi: %s\n", doi);
                CrossrefHarvesterTool.this.out.flush();
            }
        }
    }

    class ListRecordsBodyHandler implements Handler<Document>
    {
        XMLOutputter outputXML;
        int count;

        ListRecordsBodyHandler() {
            this.outputXML = new XMLOutputter(Format.getCompactFormat());
            this.count = 0;
        }

        @Override
        public void handle(final Document document) {
            final Element root = document.getRootElement();
            for (final Element record : CrossrefHarvesterTool.this.findDescendants(root, "record")) {
                final Element header = CrossrefHarvesterTool.this.findDescendant(record, "header");
                final Element metadata = CrossrefHarvesterTool.this.findDescendant(record, "metadata");
                final String datestamp = CrossrefHarvesterTool.this.findDescendantValue(header, "datestamp", "");
                final String identifier = CrossrefHarvesterTool.this.findDescendantValue(header, "identifier", "info:doi/");
                final String doi = SimpleUrlBuilder.decode(identifier).substring(9);
                CrossrefHarvesterTool.this.out.printf("record.count: %d\n", ++this.count);
                CrossrefHarvesterTool.this.out.printf("record.datestamp: %s\n", datestamp);
                CrossrefHarvesterTool.this.out.printf("record.identifier: %s\n", identifier);
                CrossrefHarvesterTool.this.out.printf("record.doi: %s\n", doi);
                CrossrefHarvesterTool.this.out.printf("record.metadata: ", new Object[0]);
                final Element metadataChild = CrossrefHarvesterTool.this.findDescendant(metadata);
                if (metadataChild != null) {
                    try {
                        this.outputXML.output(metadataChild, (OutputStream)CrossrefHarvesterTool.this.out);
                        CrossrefHarvesterTool.this.out.print("\n");
                    }
                    catch (IOException e) {
                        CrossrefHarvesterTool.this.log(Level.ERROR, e, "unable to output XML", new Object[0]);
                    }
                }
                else {
                    final String metadataText = metadata.getTextTrim();
                    if (metadataText != null) {
                        CrossrefHarvesterTool.this.out.print(metadataText);
                        CrossrefHarvesterTool.this.out.print("\n");
                    }
                    else {
                        CrossrefHarvesterTool.this.out.print("(none)\n");
                    }
                }
                CrossrefHarvesterTool.this.out.flush();
            }
        }
    }

    static class SimpleUrlBuilder
    {
        private static final Map<String, Integer> PROTOCOL_TO_DEFAULT_PORT;
        private String protocol;
        private String host;
        private int port;
        private File path;
        private Map<String, List<String>> query;
        private String reference;

        private SimpleUrlBuilder() {
            this.protocol = "http";
            this.host = "localhost";
            this.port = 80;
        }

        public SimpleUrlBuilder(final String seedUrl) throws IllegalStateException {
            this.protocol = "http";
            this.host = "localhost";
            this.port = 80;
            try {
                final URL url = new URL(seedUrl);
                this.protocol = url.getProtocol();
                this.host = url.getHost();
                this.port = url.getPort();
                this.path = new File(url.getPath());
                this.query = parseQuery(url.getQuery(), new HashMap<String, List<String>>());
                this.reference = url.getRef();
            }
            catch (MalformedURLException e) {
                throw new IllegalStateException("unable to parse url " + seedUrl, e);
            }
        }

        public SimpleUrlBuilder copy() {
            final SimpleUrlBuilder b = new SimpleUrlBuilder();
            b.setProtocol(this.protocol);
            b.setHost(this.host);
            b.setPort(this.port);
            b.setPath(this.path.getAbsolutePath());
            b.setQuery(this.query);
            b.setReference(this.reference);
            return b;
        }

        public SimpleUrlBuilder setProtocol(final String protocol) {
            this.protocol = protocol;
            return this;
        }

        public SimpleUrlBuilder setHost(final String host) {
            this.host = host;
            return this;
        }

        public SimpleUrlBuilder setPort(final int port) {
            this.port = port;
            return this;
        }

        public SimpleUrlBuilder setPath(final String path) {
            this.path = new File(path);
            return this;
        }

        public SimpleUrlBuilder setQuery(final Map<String, List<String>> query) {
            this.query = new HashMap<String, List<String>>(query);
            return this;
        }

        public SimpleUrlBuilder setReference(final String reference) {
            this.reference = reference;
            return this;
        }

        public SimpleUrlBuilder addParameter(final String name, final Object value) {
            List<String> values = this.query.get(name);
            if (values == null) {
                values = new ArrayList<String>();
                this.query.put(name, values);
            }
            values.add(value.toString());
            return this;
        }

        public SimpleUrlBuilder setParameter(final String name, final Object value) {
            List<String> values = this.query.get(name);
            if (values == null) {
                values = new ArrayList<String>();
                this.query.put(name, values);
            }
            else {
                values.clear();
            }
            values.add(value.toString());
            return this;
        }

        public List<String> findParameter(final String name) {
            List<String> values = this.query.get(name);
            if (values == null) {
                values = Collections.emptyList();
            }
            return values;
        }

        public SimpleUrlBuilder addPath(final Object... parts) {
            for (final Object part : parts) {
                this.path = new File(this.path, part.toString());
            }
            return this;
        }

        public String build() {
            final StringBuilder u = new StringBuilder();
            u.append(this.protocol);
            u.append("://");
            u.append(this.host);
            if (this.port != -1 && (!SimpleUrlBuilder.PROTOCOL_TO_DEFAULT_PORT.containsKey(this.protocol) || SimpleUrlBuilder.PROTOCOL_TO_DEFAULT_PORT.get(this.protocol) != this.port)) {
                u.append(":").append(this.port);
            }
            u.append((this.path == null) ? "/" : this.path.getAbsolutePath());
            if (!this.query.isEmpty()) {
                u.append("?");
                boolean notFirstParmater = false;
                for (final Map.Entry<String, List<String>> e : this.query.entrySet()) {
                    final String name = encode(e.getKey());
                    for (final Object v : e.getValue()) {
                        if (notFirstParmater) {
                            u.append("&");
                        }
                        u.append(name).append("=").append(encode(v.toString()));
                        notFirstParmater = true;
                    }
                }
            }
            if (this.reference != null) {
                u.append("#").append(this.reference);
            }
            return u.toString();
        }

        public static Map<String, List<String>> parseQuery(final String query, final Map<String, List<String>> parameters) {
            if (query != null) {
                final StringTokenizer pairs = new StringTokenizer(query, "&");
                while (pairs.hasMoreTokens()) {
                    final String pair = pairs.nextToken();
                    final StringTokenizer parts = new StringTokenizer(pair, "=");
                    final String name = decode(parts.nextToken());
                    final String value = parts.hasMoreTokens() ? decode(parts.nextToken()) : "";
                    List<String> values = parameters.get(name);
                    if (values == null) {
                        values = new LinkedList<String>();
                        parameters.put(name, values);
                    }
                    values.add(value);
                }
            }
            return parameters;
        }

        public static String decode(final String input) {
            try {
                return URLDecoder.decode(input, "ISO-8859-1");
            }
            catch (UnsupportedEncodingException e) {
                throw new IllegalStateException(e);
            }
        }

        public static String encode(final String input) {
            try {
                return URLEncoder.encode(input, "ISO-8859-1");
            }
            catch (UnsupportedEncodingException e) {
                throw new IllegalStateException(e);
            }
        }

        static {
            (PROTOCOL_TO_DEFAULT_PORT = new HashMap<String, Integer>()).put("http", 80);
            SimpleUrlBuilder.PROTOCOL_TO_DEFAULT_PORT.put("https", 443);
            SimpleUrlBuilder.PROTOCOL_TO_DEFAULT_PORT.put("ftp", 20);
        }
    }

    interface Handler<T>
    {
        void handle(final T p0);
    }
}